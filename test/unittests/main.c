// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include <unit.h>

IMPORT_SUITE(Canary);
IMPORT_SUITE(Matrix);
IMPORT_SUITE(Node);

BEGIN_REGISTRATION
  REGISTER(Canary)
  REGISTER(Matrix)
  REGISTER(Node)
END_REGISTRATION

UNIT_MAIN();
