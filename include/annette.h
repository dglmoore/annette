// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#pragma once

#include <stdio.h>
#include <stdlib.h>

typedef enum annette_nodetype
{
  ANNETTE_INPUT,
  ANNETTE_OUTPUT,
  ANNETTE_HIDDEN,
} annette_nodetype;

typedef struct annette annette;

annette *annette_alloc();
void annette_free(annette *net);

int annette_add_node(annette *net, annette_nodetype type, double threshold);
int annette_add_edge(annette *net, size_t a, size_t b, double weight);

int annette_node_count(annette const *net);
int annette_input_count(annette const *net);
int annette_hidden_count(annette const *net);
int annette_output_count(annette const *net);
int annette_edge_count(annette const *net);

void annette_reset(annette *node);
void annette_flush(annette *node);

int annette_is_wellconnected(annette const *net);
int annette_fire(annette *net, double const *input, double *result);

size_t annette_write(int fd, annette const *net);
size_t annette_read(int fd, annette **net);

size_t annette_fwrite(annette const *net, FILE *stream);
size_t annette_fread(annette **net, FILE *stream);
