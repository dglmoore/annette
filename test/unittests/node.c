// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include "node.h"
#include <ginger/vector.h>
#include <unit.h>

UNIT(AllocateInput)
{
  annette_node *node = annette_node_alloc(ANNETTE_INPUT, 1.0);
  ASSERT_NOT_NULL(node);

  ASSERT_NOT_NULL(node->sources);
  ASSERT_EQUAL(0, gvector_cap(node->sources));
  ASSERT_EQUAL(sizeof(annette_node*), gvector_size(node->sources));
  ASSERT_EQUAL(0, gvector_len(node->sources));

  ASSERT_NOT_NULL(node->targets);
  ASSERT_EQUAL(1, gvector_cap(node->targets));
  ASSERT_EQUAL(sizeof(annette_node*), gvector_size(node->targets));
  ASSERT_EQUAL(0, gvector_len(node->targets));

  ASSERT_NOT_NULL(node->weights);
  ASSERT_EQUAL(1, gvector_cap(node->weights));
  ASSERT_EQUAL(sizeof(annette_node*), gvector_size(node->weights));
  ASSERT_EQUAL(0, gvector_len(node->weights));

  ASSERT_EQUAL(ANNETTE_INPUT, node->type);
  ASSERT_DBL_NEAR(1.0, node->threshold);

  ASSERT_DBL_NEAR(0.0, node->stimulus);
  ASSERT_DBL_NEAR(0.0, node->state);
  ASSERT_FALSE(node->fired);
  ASSERT_FALSE(node->active);

  annette_node_free(node);
}

UNIT(AllocateOutput)
{
  annette_node *node = annette_node_alloc(ANNETTE_OUTPUT, 1.0);
  ASSERT_NOT_NULL(node);

  ASSERT_NOT_NULL(node->sources);
  ASSERT_EQUAL(1, gvector_cap(node->sources));
  ASSERT_EQUAL(sizeof(annette_node*), gvector_size(node->sources));
  ASSERT_EQUAL(0, gvector_len(node->sources));

  ASSERT_NOT_NULL(node->targets);
  ASSERT_EQUAL(0, gvector_cap(node->targets));
  ASSERT_EQUAL(sizeof(annette_node*), gvector_size(node->targets));
  ASSERT_EQUAL(0, gvector_len(node->targets));

  ASSERT_NOT_NULL(node->weights);
  ASSERT_EQUAL(0, gvector_cap(node->weights));
  ASSERT_EQUAL(sizeof(annette_node*), gvector_size(node->weights));
  ASSERT_EQUAL(0, gvector_len(node->weights));

  ASSERT_EQUAL(ANNETTE_OUTPUT, node->type);
  ASSERT_DBL_NEAR(node->threshold, 1.0);

  ASSERT_DBL_NEAR(0.0, node->stimulus);
  ASSERT_DBL_NEAR(0.0, node->state);
  ASSERT_FALSE(node->fired);
  ASSERT_FALSE(node->active);

  annette_node_free(node);
}

UNIT(AllocateHidden)
{
  annette_node *node = annette_node_alloc(ANNETTE_HIDDEN, 1.0);
  ASSERT_NOT_NULL(node);

  ASSERT_NOT_NULL(node->sources);
  ASSERT_EQUAL(1, gvector_cap(node->sources));
  ASSERT_EQUAL(sizeof(annette_node*), gvector_size(node->sources));
  ASSERT_EQUAL(0, gvector_len(node->sources));

  ASSERT_NOT_NULL(node->targets);
  ASSERT_EQUAL(1, gvector_cap(node->targets));
  ASSERT_EQUAL(sizeof(annette_node*), gvector_size(node->targets));
  ASSERT_EQUAL(0, gvector_len(node->targets));

  ASSERT_NOT_NULL(node->weights);
  ASSERT_EQUAL(1, gvector_cap(node->weights));
  ASSERT_EQUAL(sizeof(annette_node*), gvector_size(node->weights));
  ASSERT_EQUAL(0, gvector_len(node->weights));

  ASSERT_EQUAL(ANNETTE_HIDDEN, node->type);
  ASSERT_DBL_NEAR(1.0, node->threshold);

  ASSERT_DBL_NEAR(0.0, node->stimulus);
  ASSERT_DBL_NEAR(0.0, node->state);
  ASSERT_FALSE(node->fired);
  ASSERT_FALSE(node->active);

  annette_node_free(node);
}

UNIT(Reset)
{
  annette_node_reset(NULL);

  annette_node *node = annette_node_alloc(ANNETTE_INPUT, 2.0);
  ASSERT_NOT_NULL(node);
  node->stimulus = 5.0;
  node->active = true;
  node->fired = true;

  annette_node_reset(node);
  ASSERT_DBL_NEAR(0.0, node->stimulus);
  ASSERT_FALSE(node->fired);
  ASSERT_FALSE(node->active);

  annette_node_free(node);
}

UNIT(Flush)
{
  annette_node_flush(NULL);

  annette_node *node = annette_node_alloc(ANNETTE_INPUT, 2.0);
  ASSERT_NOT_NULL(node);
  node->stimulus = 5.0;
  node->state = 2.8;
  node->active = true;
  node->fired = true;

  annette_node_flush(node);
  ASSERT_DBL_NEAR(0.0, node->stimulus);
  ASSERT_DBL_NEAR(0.0, node->state);
  ASSERT_FALSE(node->fired);
  ASSERT_FALSE(node->active);

  annette_node_free(node);
}

UNIT(AddSource)
{
  annette_node *node = annette_node_alloc(ANNETTE_HIDDEN, 0.0);
  ASSERT_NOT_NULL(node);
  ASSERT_NOT_NULL(node->sources);
  ASSERT_EQUAL(1, gvector_cap(node->sources));
  ASSERT_EQUAL(0, gvector_len(node->sources));

  ASSERT_EQUAL(0, annette_node_add_source(node, node));
  ASSERT_NOT_NULL(node->sources);
  ASSERT_EQUAL(1, gvector_cap(node->sources));
  ASSERT_EQUAL(1, gvector_len(node->sources));
  ASSERT_EQUAL_P(node, node->sources[0]);

  ASSERT_EQUAL(0, annette_node_add_source(node, node));
  ASSERT_NOT_NULL(node->sources);
  ASSERT_EQUAL(2, gvector_cap(node->sources));
  ASSERT_EQUAL(2, gvector_len(node->sources));
  ASSERT_EQUAL_P(node, node->sources[0]);
  ASSERT_EQUAL_P(node, node->sources[1]);

  annette_node_free(node);
}

UNIT(AddTarget)
{
  annette_node *node = annette_node_alloc(ANNETTE_HIDDEN, 0.0);
  ASSERT_NOT_NULL(node);
  ASSERT_NOT_NULL(node->targets);
  ASSERT_EQUAL(1, gvector_cap(node->targets));
  ASSERT_EQUAL(0, gvector_len(node->targets));
  ASSERT_NOT_NULL(node->weights);
  ASSERT_EQUAL(1, gvector_cap(node->weights));
  ASSERT_EQUAL(0, gvector_len(node->weights));

  ASSERT_EQUAL(0, annette_node_add_target(node, node, 2.0));
  ASSERT_NOT_NULL(node->targets);
  ASSERT_EQUAL(1, gvector_cap(node->targets));
  ASSERT_EQUAL(1, gvector_len(node->targets));
  ASSERT_EQUAL_P(node, node->targets[0]);
  ASSERT_NOT_NULL(node->weights);
  ASSERT_EQUAL(1, gvector_cap(node->weights));
  ASSERT_EQUAL(1, gvector_len(node->weights));
  ASSERT_DBL_NEAR(2.0, node->weights[0]);

  ASSERT_EQUAL(0, annette_node_add_target(node, node, 0.5));
  ASSERT_NOT_NULL(node->targets);
  ASSERT_EQUAL(2, gvector_cap(node->targets));
  ASSERT_EQUAL(2, gvector_len(node->targets));
  ASSERT_EQUAL_P(node, node->targets[0]);
  ASSERT_EQUAL_P(node, node->targets[1]);
  ASSERT_NOT_NULL(node->weights);
  ASSERT_EQUAL(2, gvector_cap(node->weights));
  ASSERT_EQUAL(2, gvector_len(node->weights));
  ASSERT_DBL_NEAR(2.0, node->weights[0]);
  ASSERT_DBL_NEAR(0.5, node->weights[1]);

  annette_node_free(node);
}

BEGIN_SUITE(Node)
  ADD_UNIT(AllocateInput)
  ADD_UNIT(AllocateOutput)
  ADD_UNIT(AllocateHidden)
  ADD_UNIT(Reset)
  ADD_UNIT(Flush)
  ADD_UNIT(AddSource)
  ADD_UNIT(AddTarget)
END_SUITE
