// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#define _POSIX_SOURCE

#include "matrix.h"
#include <ginger/vector.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

annette *annette_alloc()
{
  annette *net = malloc(sizeof(annette));
  if (net)
  {
    net->node_count = 0;
    net->edge_count = 0;

    net->weights    = NULL;
    net->thresholds = gvector_alloc(3, 0, sizeof(double));

    net->inputs  = gvector_alloc(1, 0, sizeof(size_t));
    net->hidden  = gvector_alloc(1, 0, sizeof(size_t));
    net->outputs = gvector_alloc(1, 0, sizeof(size_t));

    net->states   = gvector_alloc(3, 0, sizeof(double));
    net->stimulus = gvector_alloc(3, 0, sizeof(double));
    net->fired    = gvector_alloc(3, 0, sizeof(bool));
    net->active   = gvector_alloc(3, 0, sizeof(bool));

    if (!net->thresholds || !net->inputs || !net->hidden   ||
        !net->outputs    || !net->states || !net->stimulus ||
        !net->fired      || !net->active)
    {
      annette_free(net);
      net = NULL;
    }
  }
  return net;
}

void annette_free(annette *net)
{
  if (net)
  {
    gvector_free(net->weights);
    gvector_free(net->thresholds);
    gvector_free(net->inputs);
    gvector_free(net->hidden);
    gvector_free(net->outputs);
    gvector_free(net->states);
    gvector_free(net->stimulus);
    gvector_free(net->fired);
    gvector_free(net->active);
    free(net);
  }
}

double get_weight(annette const *net, size_t i, size_t j)
{
  return net->weights[j + i * net->node_count];
}

double set_weight(annette *net, size_t i, size_t j, double x)
{
  return net->weights[j + i * net->node_count] = x;
}

double add_weight(annette *net, size_t i, size_t j, double x)
{
  return net->weights[j + i * net->node_count] += x;
}

int annette_add_node(annette *net, annette_nodetype type, double threshold)
{
  if (net)
  {
    size_t const n = net->node_count + 1;
    double *new_weights = gvector_alloc(n * n, n * n, sizeof(double));
    for (size_t i = 0; i < n; ++i)
    {
      for (size_t j = 0; j < n; ++j)
      {
        if (i == net->node_count || j == net->node_count)
        {
          new_weights[j + i * n] = 0.0;
        }
        else
        {
          new_weights[j + i * n] = get_weight(net, i, j);
        }
      }
    }
    double *old_weights = net->weights;
    net->weights = new_weights;
    gvector_free(old_weights);

    gvector_push(net->thresholds, threshold);
    gvector_push(net->states, 0.0);
    gvector_push(net->stimulus, 0.0);
    gvector_push(net->fired, false);
    gvector_push(net->active, false);

    if (type == ANNETTE_INPUT)
    {
      gvector_push(net->inputs, net->node_count);
    }
    else if (type == ANNETTE_OUTPUT)
    {
      gvector_push(net->outputs, net->node_count);
    }
    else
    {
      gvector_push(net->hidden, net->node_count);
    }

    annette_reset(net);

    return net->node_count++;
  }
  return -1;
}

int annette_add_edge(annette *net, size_t a, size_t b, double weight)
{
  if (net && a < net->node_count && b < net->node_count)
  {
    add_weight(net, b, a, weight);
    return ++net->edge_count;
  }
  return -1;
}

int annette_node_count(annette const *net)
{
  return net->node_count;
}

int annette_input_count(annette const *net)
{
  return gvector_len(net->inputs);
}

int annette_hidden_count(annette const *net)
{
  return gvector_len(net->hidden);
}

int annette_output_count(annette const *net)
{
  return gvector_len(net->outputs);
}

int annette_edge_count(annette const *net)
{
  return net->edge_count;
}

void annette_reset(annette *net)
{
  if (net)
  {
    for (size_t i = 0; i < net->node_count; ++i)
    {
      net->stimulus[i] = 0.0;
      net->active[i] = false;
      net->fired[i] = false;
    }
  }
}

void annette_flush(annette *net)
{
  if (net)
  {
    annette_reset(net);
    for (size_t i = 0; i < net->node_count; ++i)
    {
      net->states[i] = 0.0;
    }
  }
}

static bool fully_fired(annette const *net)
{
  if (net && net->outputs)
  {
    for (size_t i = 0; i < gvector_len(net->outputs); ++i)
    {
      if (!net->fired[net->outputs[i]])
      {
        return false;
      }
    }
  }
  return true;
}

bool activate_node(annette *net, size_t idx, double x)
{
  if (net && idx < net->node_count)
  {
    net->stimulus[idx] = x;
    net->active[idx] = false;
    for (size_t i = 0; i < net->node_count; ++i)
    {
      double weight = net->weights[i + idx * net->node_count];
      if (weight)
      {
        net->stimulus[idx] += weight * net->states[i];
        if (net->fired[i])
        {
          net->active[idx] = true;
        }
      }
    }
    return net->active[idx];
  }
  return false;
}

double fire_node(annette *net, size_t idx)
{
  if (net && idx < net->node_count)
  {
    net->fired[idx] = true;
    net->states[idx] = (net->stimulus[idx] <= net->thresholds[idx]) ? 0.0 : 1.0;
    return net->states[idx];
  }
  return 0.0;
}

double fire_node_with(annette *net, size_t idx, double x)
{
  if (net && idx < net->node_count)
  {
    activate_node(net, idx, x);
    net->active[idx] = true;
    return fire_node(net, idx);
  }
  return 0.0;
}

int annette_is_wellconnected(annette const *net)
{
  if (net)
  {
    for (size_t i = 0; i < gvector_len(net->inputs); ++i)
    {
      size_t k = net->inputs[i];
      int valid_input = 0;
      for (size_t j = 0; j < net->node_count; ++j)
      {
        if (k != j && get_weight(net, j, k))
        {
          valid_input = 1;
          break;
        }
      }
      if (!valid_input) return false;
    }

    for (size_t i = 0; i < gvector_len(net->outputs); ++i)
    {
      size_t k = net->outputs[i];
      int valid_output = 0;
      for (size_t j = 0; j < net->node_count; ++j)
      {
        if (k != j && get_weight(net, k, j))
        {
          valid_output = 1;
          break;
        }
      }
      if (!valid_output) return false;
    }

    for (size_t i = 0; i < gvector_len(net->hidden); ++i)
    {
      size_t k = net->hidden[i];
      int valid_input = 0, valid_output = 0;
      for (size_t j = 0; j < net->node_count; ++j)
      {
        if (k != j)
        {
          if (get_weight(net, j, k))
          {
            valid_input = 1;
          }
          if (get_weight(net, k, j))
          {
            valid_output = 1;
          }
        }
        if (valid_input && valid_output) break;
      }
      if (!valid_input || !valid_output) return false;
    }
    return true;
  }
  return false;
}

int annette_fire(annette *net, double const *input, double *result)
{
  if (net && input && result)
  {
    bool first_time = true;
    int loop_count = 0;

    for (size_t i = 0; i < gvector_len(net->inputs); ++i)
    {
      fire_node_with(net, net->inputs[i], input[i]);
    }

    while (!fully_fired(net) || first_time)
    {
      ++loop_count;
      if (loop_count >= 20)
      {
        return -2;
      }

      for (size_t i = 0; i < net->node_count; ++i)
      {
        if (!net->active[i])
        {
          activate_node(net, i, 0.0);
        }
      }

      for (size_t i = 0; i < net->node_count; ++i)
      {
        if (net->active[i] && !net->fired[i])
        {
          fire_node(net, i);
        }
      }

      first_time = false;
    }

    for (size_t i = 0; i < gvector_len(net->outputs); ++i)
    {
      if (!net->fired[net->outputs[i]])
      {
        return -3;
      }
    }

    annette_reset(net);

    for (size_t i = 0; i < gvector_len(net->outputs); ++i)
    {
      result[i] = net->states[net->outputs[i]];
    }
    return 0;
  }
  return -1;
}

size_t gvector_write(int fd, gvector_const vec)
{
  if (vec)
  {
    size_t bytes = 0;
    bytes += write(fd, &gvector_size(vec), sizeof(size_t));
    bytes += write(fd, &gvector_len(vec), sizeof(size_t));
    bytes += write(fd, vec, gvector_size(vec)*gvector_len(vec));
    return bytes;
  }
  return 0;
}

size_t gvector_read(int fd, gvector *vec)
{
  if (vec)
  {
    gvector_free(*vec);

    size_t bytes = 0, size = 0, len = 0;

    bytes += read(fd, &size, sizeof(size_t));
    bytes += read(fd, &len, sizeof(size_t));

    *vec = gvector_alloc(len, len, size);
    bytes += read(fd, *vec, size * len);

    return bytes;
  }
  return 0;
}

size_t annette_write(int fd, annette const *net)
{
  if (net)
  {
    size_t bytes = 0;
    bytes += write(fd, &net->node_count, sizeof(net->node_count));
    bytes += write(fd, &net->edge_count, sizeof(net->edge_count));
    bytes += gvector_write(fd, net->weights);
    bytes += gvector_write(fd, net->thresholds);
    bytes += gvector_write(fd, net->inputs);
    bytes += gvector_write(fd, net->hidden);
    bytes += gvector_write(fd, net->outputs);
    bytes += gvector_write(fd, net->stimulus);
    bytes += gvector_write(fd, net->states);
    bytes += gvector_write(fd, net->fired);
    bytes += gvector_write(fd, net->active);

    size_t n = net->node_count;

    size_t expected_bytes = 0;
    expected_bytes += (n + 3) * n * sizeof(double);
    expected_bytes += (n + 20) * sizeof(size_t);
    expected_bytes += 2 * n * sizeof(bool);

    assert(expected_bytes == bytes);

    return bytes;
  }
  return 0;
}

size_t annette_read(int fd, annette **net)
{
  if (net)
  {
    annette_free(*net);
    *net = annette_alloc();
    if (!*net) return 0;

    size_t bytes = 0;
    bytes += read(fd, &(*net)->node_count, sizeof(size_t));
    bytes += read(fd, &(*net)->edge_count, sizeof(size_t));
    bytes += gvector_read(fd, (void**)&((*net)->weights));
    bytes += gvector_read(fd, (void**)&((*net)->thresholds));
    bytes += gvector_read(fd, (void**)&((*net)->inputs));
    bytes += gvector_read(fd, (void**)&((*net)->hidden));
    bytes += gvector_read(fd, (void**)&((*net)->outputs));
    bytes += gvector_read(fd, (void**)&((*net)->stimulus));
    bytes += gvector_read(fd, (void**)&((*net)->states));
    bytes += gvector_read(fd, (void**)&((*net)->fired));
    bytes += gvector_read(fd, (void**)&((*net)->active));
    return bytes;
}
  return 0;
}

size_t annette_fwrite(annette const *net, FILE *stream)
{
  if (net && stream)
  {
    int fd = fileno(stream);
    return annette_write(fd, net);
  }
  return 0;
}

size_t annette_fread(annette **net, FILE *stream)
{
  if (net && stream)
  {
    int fd = fileno(stream);
    return annette_read(fd, net);
  }
  return 0;
}
