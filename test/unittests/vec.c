// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include "vec.h"
#include <unit.h>

UNIT(AllocateZeroSize)
{
  ASSERT_NULL(vec_alloc(0, 0, 0));
  ASSERT_NULL(vec_alloc(5, 0, 0));
  ASSERT_NULL(vec_alloc(0, 5, 0));
  ASSERT_NULL(vec_alloc(5, 5, 0));
}

UNIT(AllocateZero)
{
  int *v = vec_alloc(0, 0, sizeof(int));
  ASSERT_NOT_NULL(v);
  ASSERT_EQUAL(0, vec_cap(v));
  ASSERT_EQUAL(sizeof(int), vec_size(v));
  ASSERT_EQUAL(0, vec_len(v));
  vec_free(v);
}

UNIT(AllocateOne)
{
  int *v = vec_alloc(1, 0, sizeof(int));
  ASSERT_NOT_NULL(v);
  ASSERT_EQUAL(1, vec_cap(v));
  ASSERT_EQUAL(sizeof(int), vec_size(v));
  ASSERT_EQUAL(0, vec_len(v));
  vec_free(v);

  v = vec_alloc(1, 1, sizeof(int));
  ASSERT_NOT_NULL(v);
  ASSERT_EQUAL(1, vec_cap(v));
  ASSERT_EQUAL(sizeof(int), vec_size(v));
  ASSERT_EQUAL(1, vec_len(v));

  v = vec_alloc(1, 2, sizeof(int));
  ASSERT_NOT_NULL(v);
  ASSERT_EQUAL(1, vec_cap(v));
  ASSERT_EQUAL(sizeof(int), vec_size(v));
  ASSERT_EQUAL(1, vec_len(v));
}

UNIT(ReserveGrow)
{
  size_t const N = 2;

  int *v = vec_alloc(N, N, sizeof(int));
  for (size_t i = 0; i < N; ++i) v[i] = (int)i;
  ASSERT_NOT_NULL(v);

  int *w = vec_reserve(v, 3*N);
  ASSERT_NOT_NULL(w);
  ASSERT_EQUAL(6, vec_cap(w));
  ASSERT_EQUAL(sizeof(int), vec_size(w));
  ASSERT_EQUAL(N, vec_len(w));
  for (size_t i = 0; i < N; ++i) ASSERT_EQUAL(i, w[i]);

  vec_free(w);
}

UNIT(ReserveShrink)
{
  size_t const N = 2;
  int *v = vec_alloc(3*N, 3*N, sizeof(int));
  for (size_t i = 0; i < 3*N; ++i) v[i] = (int)i;
  ASSERT_NOT_NULL(v);

  int *w = vec_reserve(v, N);
  ASSERT_NOT_NULL(w);
  ASSERT_EQUAL(N, vec_cap(w));
  ASSERT_EQUAL(sizeof(int), vec_size(w));
  ASSERT_EQUAL(N, vec_len(w));
  for (size_t i = 0; i < N; ++i) ASSERT_EQUAL(i, w[i]);

  vec_free(w);
}

UNIT(ReserveZero)
{
  size_t const N = 2;
  int *v = vec_alloc(3*N, 3*N, sizeof(int));
  for (size_t i = 0; i < 3*N; ++i) v[i] = (int)i;
  ASSERT_NOT_NULL(v);

  int *w = vec_reserve(v, 0);
  ASSERT_NOT_NULL(w);
  ASSERT_EQUAL(0, vec_cap(w));
  ASSERT_EQUAL(sizeof(int), vec_size(w));
  ASSERT_EQUAL(0, vec_len(w));

  vec_free(w);
}

UNIT(ReserveNULL)
{
  ASSERT_NULL(vec_reserve(NULL, 5));
}

UNIT(PushSuccess)
{
  int *v = vec_alloc(3, 0, sizeof(int));
  ASSERT_NOT_NULL(v);
  vec_push(v,1);
  vec_push(v,2);
  vec_push(v,3);
  ASSERT_EQUAL(3, vec_cap(v));
  ASSERT_EQUAL(3, vec_len(v));
  for (size_t i = 0; i < 3; ++i) ASSERT_EQUAL(i+1, v[i]);
  vec_free(v);
}

UNIT(PushFailure)
{
  int *v = NULL;
  ASSERT_SIGNAL(SIGSEGV, vec_push(v,0));

  v = vec_alloc(0, 0, sizeof(int));
  ASSERT_NOT_NULL(v);
  ASSERT_SIGNAL(SIGSEGV, vec_push(v,1));
  vec_free(v);
}

UNIT(Pop)
{
  int *v = vec_alloc(3, 3, sizeof(int));
  ASSERT_NOT_NULL(v);

  vec_pop(v);
  ASSERT_EQUAL(3, vec_cap(v));
  ASSERT_EQUAL(2, vec_len(v));

  vec_pop(v);
  ASSERT_EQUAL(3, vec_cap(v));
  ASSERT_EQUAL(1, vec_len(v));

  vec_pop(v);
  ASSERT_EQUAL(3, vec_cap(v));
  ASSERT_EQUAL(0, vec_len(v));

  vec_pop(v);
  ASSERT_EQUAL(3, vec_cap(v));
  ASSERT_EQUAL(0, vec_len(v));

  vec_free(v);
}

UNIT(IsEmpty)
{
  ASSERT_TRUE(vec_isempty(NULL));

  int *v = NULL;
  ASSERT_TRUE(vec_isempty(v));

  v = vec_alloc(3, 0, sizeof(int));
  ASSERT_TRUE(vec_isempty(v));

  vec_push(v, 1);
  ASSERT_FALSE(vec_isempty(v));

  vec_free(v);
}

UNIT(Shrink)
{
  int *v = vec_alloc(5, 2, sizeof(int));
  ASSERT_NOT_NULL(v);
  ASSERT_EQUAL(5, vec_cap(v));
  ASSERT_EQUAL(sizeof(int), vec_size(v));
  ASSERT_EQUAL(2, vec_len(v));

  int *w = vec_shrink(v);
  ASSERT_NOT_NULL(w);
  ASSERT_EQUAL(2, vec_cap(w));
  ASSERT_EQUAL(sizeof(int), vec_size(w));
  ASSERT_EQUAL(2, vec_len(w));

  vec_len(w) = 0;
  ASSERT_NOT_NULL(w);
  ASSERT_EQUAL(2, vec_cap(w));
  ASSERT_EQUAL(sizeof(int), vec_size(w));
  ASSERT_EQUAL(0, vec_len(w));

  v = vec_shrink(w);
  ASSERT_NOT_NULL(v);
  ASSERT_EQUAL(0, vec_cap(v));
  ASSERT_EQUAL(sizeof(int), vec_size(v));
  ASSERT_EQUAL(0, vec_len(v));

  vec_free(v);
}

BEGIN_SUITE(Vector)
  ADD_UNIT(AllocateZeroSize)
  ADD_UNIT(AllocateZero)
  ADD_UNIT(AllocateOne)
  ADD_UNIT(ReserveGrow)
  ADD_UNIT(ReserveShrink)
  ADD_UNIT(ReserveZero)
  ADD_UNIT(ReserveNULL)
  ADD_UNIT(PushSuccess)
  ADD_UNIT(PushFailure)
  ADD_UNIT(Pop)
  ADD_UNIT(IsEmpty)
  ADD_UNIT(Shrink)
END_SUITE
