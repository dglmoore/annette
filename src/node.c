// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include "node.h"
#include <ginger/vector.h>
#include <assert.h>

annette_node *annette_node_alloc(annette_nodetype type, double threshold)
{
  annette_node *node = malloc(sizeof(annette_node));
  if (node)
  {
    node->sources = gvector_alloc(type != ANNETTE_INPUT, 0, sizeof(annette_node*));
    node->targets = gvector_alloc(type != ANNETTE_OUTPUT, 0, sizeof(annette_node*));
    node->weights = gvector_alloc(type != ANNETTE_OUTPUT, 0, sizeof(annette_node*));

    if (node->sources && node->targets && node->weights)
    {
      node->type = type;
      node->threshold = threshold;

      node->stimulus = 0.0;
      node->state = 0.0;
      node->fired = false;
      node->active = false;
    }
    else
    {
      annette_node_free(node);
      node = NULL;
    }
  }
  return node;
}

void annette_node_free(annette_node *node)
{
  if (node)
  {
    gvector_free(node->sources);
    gvector_free(node->targets);
    gvector_free(node->weights);
    free(node);
  }
}

void annette_node_reset(annette_node *node)
{
  if (node)
  {
    node->stimulus = 0.0;
    node->active = false;
    node->fired = false;
  }
}

void annette_node_flush(annette_node *node)
{
  if (node)
  {
    annette_node_reset(node);
    node->state = 0.0;
  }
}

int annette_node_add_source(annette_node *node, annette_node *source)
{
  if (node && source)
  {
    if (gvector_len(node->sources) >= gvector_cap(node->sources))
    {
      annette_node **srcs = gvector_reserve(node->sources, 2 * gvector_len(node->sources));
      if (!srcs)
      {
        return -1;
      }
      node->sources = srcs;
    }
    gvector_push(node->sources, source);
    return 0;
  }
  return 1;
}

int annette_node_add_target(annette_node *node, annette_node *target, double weight)
{
  if (node && target)
  {
    assert(gvector_len(node->targets) == gvector_len(node->weights));

    if (gvector_len(node->targets) >= gvector_cap(node->targets))
    {
      annette_node **targets = gvector_reserve(node->targets, 2 * gvector_len(node->targets));
      if (!targets)
      {
        return -1;
      }
      node->targets = targets;
    }

    if (gvector_len(node->weights) >= gvector_cap(node->weights))
    {
      double *weights = gvector_reserve(node->weights, 2 * gvector_len(node->weights));
      if (!weights)
      {
        return -2;
      }
      node->weights = weights;
    }

    gvector_push(node->targets, target);
    gvector_push(node->weights, weight);
    return 0;
  }
  return 1;
}

bool annette_node_activate(annette_node *node, double x)
{
  if (node)
  {
    node->stimulus = x;
    node->active = false;
    for (size_t i = 0; i < gvector_len(node->sources); ++i)
    {
      annette_node *source = node->sources[i];
      for (size_t j = 0; j < gvector_len(source->targets); ++j)
      {
        if (source->targets[j] == node)
        {
          node->stimulus += source->weights[j] * source->state;
        }
      }
      if (source->fired)
      {
        node->active = true;
      }
    }
    return node->active;
  }
  return false;
}

double annette_node_fire(annette_node *node)
{
  if (node)
  {
    node->fired = true;
    node->state = (node->stimulus <= node->threshold) ? 0.0 : 1.0;
    return node->state;
  }
  return 0.0;
}

double annette_node_fire_with(annette_node *node, double x)
{
  if (node)
  {
    annette_node_activate(node, x);
    node->active = true;
    return annette_node_fire(node);
  }
  return 0.0;
}
