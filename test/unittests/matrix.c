// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include "matrix.h"
#include <ginger/vector.h>
#include <annette.h>
#include <unit.h>

UNIT(Allocate)
{
  annette *net = annette_alloc();
  ASSERT_NOT_NULL(net);

  ASSERT_EQUAL(0, net->node_count);
  ASSERT_EQUAL(0, net->edge_count);

  ASSERT_NULL(net->weights);

  ASSERT_NOT_NULL(net->thresholds);
  ASSERT_EQUAL(3, gvector_cap(net->thresholds));
  ASSERT_EQUAL(sizeof(double), gvector_size(net->thresholds));
  ASSERT_EQUAL(0, gvector_len(net->thresholds));

  ASSERT_NOT_NULL(net->inputs);
  ASSERT_EQUAL(1, gvector_cap(net->inputs));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(net->inputs));
  ASSERT_EQUAL(0, gvector_len(net->inputs));

  ASSERT_NOT_NULL(net->hidden);
  ASSERT_EQUAL(1, gvector_cap(net->hidden));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(net->hidden));
  ASSERT_EQUAL(0, gvector_len(net->hidden));

  ASSERT_NOT_NULL(net->outputs);
  ASSERT_EQUAL(1, gvector_cap(net->outputs));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(net->outputs));
  ASSERT_EQUAL(0, gvector_len(net->outputs));

  ASSERT_NOT_NULL(net->states);
  ASSERT_EQUAL(3, gvector_cap(net->states));
  ASSERT_EQUAL(sizeof(double), gvector_size(net->states));
  ASSERT_EQUAL(0, gvector_len(net->states));

  ASSERT_NOT_NULL(net->stimulus);
  ASSERT_EQUAL(3, gvector_cap(net->stimulus));
  ASSERT_EQUAL(sizeof(double), gvector_size(net->stimulus));
  ASSERT_EQUAL(0, gvector_len(net->stimulus));

  ASSERT_NOT_NULL(net->fired);
  ASSERT_EQUAL(3, gvector_cap(net->fired));
  ASSERT_EQUAL(sizeof(bool), gvector_size(net->fired));
  ASSERT_EQUAL(0, gvector_len(net->fired));

  ASSERT_NOT_NULL(net->active);
  ASSERT_EQUAL(3, gvector_cap(net->active));
  ASSERT_EQUAL(sizeof(bool), gvector_size(net->active));
  ASSERT_EQUAL(0, gvector_len(net->active));

  annette_free(net);
}

UNIT(AddNodeNull)
{
  ASSERT_EQUAL(-1, annette_add_node(NULL, ANNETTE_INPUT, 2.0));
}

UNIT(AddNode)
{
  annette *net = annette_alloc();
  ASSERT_NOT_NULL(net);

  ASSERT_EQUAL(0, annette_add_node(net, ANNETTE_INPUT, 1.0));
  ASSERT_EQUAL(1, annette_add_node(net, ANNETTE_HIDDEN, 1.0));
  ASSERT_EQUAL(2, annette_add_node(net, ANNETTE_OUTPUT, 1.0));

  ASSERT_EQUAL(9, gvector_len(net->weights));
  ASSERT_EQUAL(3, gvector_len(net->thresholds));
  ASSERT_EQUAL(1, gvector_len(net->inputs));
  ASSERT_EQUAL(1, gvector_len(net->hidden));
  ASSERT_EQUAL(1, gvector_len(net->outputs));
  ASSERT_EQUAL(3, gvector_len(net->states));
  ASSERT_EQUAL(3, gvector_len(net->stimulus));
  ASSERT_EQUAL(3, gvector_len(net->fired));
  ASSERT_EQUAL(3, gvector_len(net->active));

  ASSERT_EQUAL(3, annette_add_node(net, ANNETTE_INPUT, 1.0));
  ASSERT_EQUAL(4, annette_add_node(net, ANNETTE_INPUT, 1.0));

  ASSERT_EQUAL(25, gvector_len(net->weights));
  ASSERT_EQUAL(5, gvector_len(net->thresholds));
  ASSERT_EQUAL(3, gvector_len(net->inputs));
  ASSERT_EQUAL(1, gvector_len(net->hidden));
  ASSERT_EQUAL(1, gvector_len(net->outputs));
  ASSERT_EQUAL(5, gvector_len(net->states));
  ASSERT_EQUAL(5, gvector_len(net->stimulus));
  ASSERT_EQUAL(5, gvector_len(net->fired));
  ASSERT_EQUAL(5, gvector_len(net->active));

  ASSERT_EQUAL(5, annette_add_node(net, ANNETTE_OUTPUT, 1.0));
  ASSERT_EQUAL(6, annette_add_node(net, ANNETTE_OUTPUT, 1.0));

  ASSERT_EQUAL(49, gvector_len(net->weights));
  ASSERT_EQUAL(7, gvector_len(net->thresholds));
  ASSERT_EQUAL(3, gvector_len(net->inputs));
  ASSERT_EQUAL(1, gvector_len(net->hidden));
  ASSERT_EQUAL(3, gvector_len(net->outputs));
  ASSERT_EQUAL(7, gvector_len(net->states));
  ASSERT_EQUAL(7, gvector_len(net->stimulus));
  ASSERT_EQUAL(7, gvector_len(net->fired));
  ASSERT_EQUAL(7, gvector_len(net->active));

  ASSERT_EQUAL(7, annette_add_node(net, ANNETTE_HIDDEN, 1.0));
  ASSERT_EQUAL(8, annette_add_node(net, ANNETTE_HIDDEN, 1.0));

  ASSERT_EQUAL(81, gvector_len(net->weights));
  ASSERT_EQUAL(9, gvector_len(net->thresholds));
  ASSERT_EQUAL(3, gvector_len(net->inputs));
  ASSERT_EQUAL(3, gvector_len(net->hidden));
  ASSERT_EQUAL(3, gvector_len(net->outputs));
  ASSERT_EQUAL(9, gvector_len(net->states));
  ASSERT_EQUAL(9, gvector_len(net->stimulus));
  ASSERT_EQUAL(9, gvector_len(net->fired));
  ASSERT_EQUAL(9, gvector_len(net->active));

  annette_free(net);
}

UNIT(AddEdge)
{
  annette *net = annette_alloc();
  ASSERT_NOT_NULL(net);

  size_t a = annette_add_node(net, ANNETTE_INPUT, 1.0);
  size_t b = annette_add_node(net, ANNETTE_OUTPUT, -1.0);
  size_t c = annette_add_node(net, ANNETTE_HIDDEN, 0.5);

  ASSERT_EQUAL(9, gvector_len(net->weights));
  ASSERT_EQUAL(3, gvector_len(net->thresholds));
  ASSERT_EQUAL(1, gvector_len(net->inputs));
  ASSERT_EQUAL(1, gvector_len(net->hidden));
  ASSERT_EQUAL(1, gvector_len(net->outputs));
  ASSERT_EQUAL(3, gvector_len(net->states));
  ASSERT_EQUAL(3, gvector_len(net->stimulus));
  ASSERT_EQUAL(3, gvector_len(net->fired));
  ASSERT_EQUAL(3, gvector_len(net->active));
  ASSERT_EQUAL(0, net->edge_count);

  ASSERT_EQUAL(1, annette_add_edge(net, a, b, 0.2));
  ASSERT_EQUAL(1, net->edge_count);
  {
    for (size_t i = 0; i < net->node_count; ++i)
    {
      for (size_t j = 0; j < net->node_count; ++j)
      {
        if (i == a && j == b)
        {
          ASSERT_DBL_NEAR(0.2, net->weights[i + j * net->node_count]);
        }
        else
        {
          ASSERT_DBL_NEAR(0.0, net->weights[i + j * net->node_count]);
        }
      }
    }
  }

  ASSERT_EQUAL(2, annette_add_edge(net, a, c, 0.5));
  ASSERT_EQUAL(3, annette_add_edge(net, c, b, -0.3));
  ASSERT_EQUAL(3, net->edge_count);
  {
    for (size_t i = 0; i < net->node_count; ++i)
    {
      for (size_t j = 0; j < net->node_count; ++j)
      {
        if (i == a && j == b)
        {
          ASSERT_DBL_NEAR(0.2, net->weights[i + j * net->node_count]);
        }
        else if (i == a && j == c)
        {
          ASSERT_DBL_NEAR(0.5, net->weights[i + j * net->node_count]);
        }
        else if (i == c && j == b)
        {
          ASSERT_DBL_NEAR(-0.3, net->weights[i + j * net->node_count]);
        }
        else
        {
          ASSERT_DBL_NEAR(0.0, net->weights[i + j * net->node_count]);
        }
      }
    }
  }

  annette_free(net);
}

UNIT(FeedForward)
{
  annette *net = annette_alloc();
  ASSERT_NOT_NULL(net);
  int a = annette_add_node(net, ANNETTE_INPUT, 0.5);
  int b = annette_add_node(net, ANNETTE_OUTPUT, 0.5);
  ASSERT_TRUE(a >= 0 && b >= 0);
  ASSERT_EQUAL(1, annette_add_edge(net, a, b, 2.0));

  double input = 0.25, output = 2.0;
  ASSERT_EQUAL(0, annette_fire(net, &input, &output));
  ASSERT_DBL_NEAR(0.0, output);

  input = 0.75;
  ASSERT_EQUAL(0, annette_fire(net, &input, &output));
  ASSERT_DBL_NEAR(1.0, output);

  annette_free(net);
}

UNIT(Recurrent)
{
  annette *net = annette_alloc();
  ASSERT_NOT_NULL(net);
  int a = annette_add_node(net, ANNETTE_INPUT, 0.0);
  int b = annette_add_node(net, ANNETTE_HIDDEN, 0.0);
  int c = annette_add_node(net, ANNETTE_HIDDEN, 0.0);
  int d = annette_add_node(net, ANNETTE_OUTPUT, 0.0);
  ASSERT_TRUE(a >= 0 && b >= 0 && c >= 0 && d >= 0);
  ASSERT_EQUAL(1, annette_add_edge(net, a, b,  1.0));
  ASSERT_EQUAL(2, annette_add_edge(net, b, c,  1.0));
  ASSERT_EQUAL(3, annette_add_edge(net, c, d,  1.0));
  ASSERT_EQUAL(4, annette_add_edge(net, c, b, -1.0));

  for (double input = 0.0; input <= 10.0; ++input)
  {
    double output = 2.0;
    ASSERT_EQUAL(0, annette_fire(net, &input, &output));
    if ((int)input % 2)
    {
      ASSERT_DBL_NEAR(1.0, output);
    }
    else
    {
      ASSERT_DBL_NEAR(0.0, output);
    }
  }

  annette_free(net);
}

UNIT(IsWellConnected)
{
  annette *net = annette_alloc();
  ASSERT_NOT_NULL(net);
  int a = annette_add_node(net, ANNETTE_INPUT, 0.0);
  int b = annette_add_node(net, ANNETTE_OUTPUT, 0.0);
  ASSERT_FALSE(annette_is_wellconnected(net));
  annette_add_edge(net, a, b, 0.5);
  ASSERT_TRUE(annette_is_wellconnected(net));

  int c = annette_add_node(net, ANNETTE_HIDDEN, 0.0);
  ASSERT_FALSE(annette_is_wellconnected(net));
  annette_add_edge(net, a, c, 0.5);
  ASSERT_FALSE(annette_is_wellconnected(net));
  annette_add_edge(net, c, b, 0.5);
  ASSERT_TRUE(annette_is_wellconnected(net));

  int d = annette_add_node(net, ANNETTE_HIDDEN, 0.0);
  ASSERT_FALSE(annette_is_wellconnected(net));
  annette_add_edge(net, d, b, 0.5);
  ASSERT_FALSE(annette_is_wellconnected(net));
  annette_add_edge(net, c, d, 0.5);
  ASSERT_TRUE(annette_is_wellconnected(net));

  int e = annette_add_node(net, ANNETTE_INPUT, 0.0);
  ASSERT_FALSE(annette_is_wellconnected(net));
  annette_add_edge(net, e, d, 0.5);
  ASSERT_TRUE(annette_is_wellconnected(net));

  annette_free(net);
}

BEGIN_SUITE(Matrix)
  ADD_UNIT(Allocate)
  ADD_UNIT(AddNodeNull)
  ADD_UNIT(AddNode)
  ADD_UNIT(AddEdge)
  ADD_UNIT(FeedForward)
  ADD_UNIT(Recurrent)
  ADD_UNIT(IsWellConnected)
END_SUITE
