// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#pragma once

#include <annette.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct annette_node
{
  annette_nodetype type;
  double threshold;
  struct annette_node **sources;
  struct annette_node **targets;
  double *weights;

  double stimulus;
  double state;
  bool fired;
  bool active;
} annette_node;

annette_node *annette_node_alloc(annette_nodetype type, double threshold);
void annette_node_free(annette_node *node);

void annette_node_reset(annette_node *node);
void annette_node_flush(annette_node *node);

int annette_node_add_source(annette_node *node, annette_node *source);
int annette_node_add_target(annette_node *node, annette_node *target, double weight);

bool annette_node_activate(annette_node *node, double x);
double annette_node_fire(annette_node *node);
double annette_node_fire_with(annette_node *node, double x);
