// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#pragma once

#include <annette.h>
#include <stdbool.h>

struct annette
{
  size_t node_count;
  size_t edge_count;

  double *weights;
  double *thresholds;

  size_t *inputs;
  size_t *hidden;
  size_t *outputs;

  double *stimulus;
  double *states;

  bool *fired;
  bool *active;
};
